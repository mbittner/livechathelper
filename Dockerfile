FROM ubuntu:14.04

RUN set -xe \
    && apt-get update \
    && apt-get -y upgrade \
    && apt-get -y install wget unzip php5-gd php5-cli php5-curl libapache2-mod-php5 php5-mysql apache2 \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

RUN set -xe \
    && wget -O /tmp/livehelperchat.zip https://github.com/remdex/livehelperchat/archive/master.zip \
    && cd /tmp \
    && unzip livehelperchat.zip

WORKDIR /tmp
RUN set -xe \
    && mv livehelperchat-master/lhc_web livehelperchat-master/html \
    && rm -rf /var/www/html \
    && mv livehelperchat-master/html /var/www/ \
    && rm -rf livehelperchat-master

WORKDIR /var/www
RUN set -xe \
    && chown -R www-data:www-data /var/www/html

COPY files/apache2-foreground /usr/local/bin/
RUN chmod a+x /usr/local/bin/apache2-foreground

EXPOSE 80

CMD ["/usr/local/bin/apache2-foreground"]
